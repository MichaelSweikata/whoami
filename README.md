# whoami

Greetings!

My name is Mike Sweikata, and welcome to my Gitlab page.

I thought it might be beneficial to start tracking content in a centralized location, as it would help build an identity for myself in terms of who Mike Sweikata is in his professional life. 

You'll find a copy of my resume under [Michael Sweikata 2025](./Michael Sweikata Cloud 2025.pdf), which is the very professional thing to provide people. And if you're interested in me as a Manager or leader, here's a [resume more tailored for that](./MichaelSweikata-EngineeringManager 2025.pdf). However, for a more open perspective of what I like to do...

# Solving problems

I like to solve problems. I'm an engineer, and part of being an engineer is looking at something and going, "How does that work?" And when you see things that don't work, you go, "Wait, how can we fix that?" So when it comes to understanding process flows, say, "When I hire a new person, how do I know that they get the access they need" to "When I push code into my repository, how do I know that it'll immediately build and trigger out a load test where fifty thousand clients are simultaneously going to try to pull the same cat picture with it?" 

# Making things easier

Something that tends to cross paths with problems is complexity. Not everything in the universe can be simple--much to my dismay. However, things can generally be put in a specific order or process to _make_ them be easier. So when it comes to solving problems, the best way to solve a problem is to make it easy. 

As I write this, I'm starting the process of redoing my garage into a workshop. Woodworking is my hobby, and I struggle because I don't have a great place to do any work. I have a 3/4 car garage now, but, it's an accessible room that stores junk haphazardly. I recognize that in order to make sure my workshop becomes the best it can be with limited space, it needs to be easy to use:

- Tools need to be accessible, easy to access and easy to store
- Charging stations and batteries need to be obvious and numerous
- Large tools need to be accessible and not have anything that blocks the in-feed/out-feed flows
- Hand tools need to be stored where their sizes are obvious
- Material storage needs to not be on the floor

So these considerations become the running rules for how I design my workshop. We also identify what other current issues hinder it's use:

- During the wintertime, it's freezing; during the summertime, it's too hot
- There is no dust collection
- There is no work bench, just a large table that stores _junk_

Combine the two lists of things, and we have our requirements. And if we meet our requirements, the workshop becomes _easy_. It now no longer becomes a difficulty in usage of a system, but an element to enable other things to occur. 

Ask me how well I did at solving the problem, I'll know in a few weeks time. 

**Spoiler alert**: Time became a problem; the work was finished but time prevented me from dealing with the specific problems. It's still an ongoing task!

# Automation

I'm genuinely in awe in our capabilities of technology. One of the things that I appreciate is our ability to automate nearly everything. Chances are if we can't automate it, it's because we haven't built the right interface for it yet.

I got my start in working with automation toolkits when I decided that configuring 800+ switchports on a network switch stack was just...boring. At the time, we called it `Networking via Excel`. Just use Microsoft Excel, concatenate the fields in a row across multiple columns, and you can turn a bunch of cells with numbers into a full fledged interface config:

| Switch | Interface | Access VLAN | Voice VLAN | Config |  |  | Templates |
|---|---|---|---|---|---|---|---|
| 1 | 1 | 20 | 99 | int Gi1/0/1 <br /> switchport mode access <br /> switchport access vlan 20 <br /> switchport voice vlan 99 <br /> exit |  |  | int Gi  <br /> switchport mode access  <br />switchport access vlan  <br />switchport voice vlan <br /> exit |
| 1 | 2 | 22 | 99 | int Gi1/0/2 <br /> switchport mode access <br /> switchport access vlan 22 <br /> switchport voice vlan 99 <br /> exit |  |  |  |
| 1 | 3 | 23 | 99 | int Gi1/0/3 <br /> switchport mode access <br /> switchport access vlan 20 <br /> switchport voice vlan 99 <br /> exit |  |  |  |

This was of course before Ansible and the like existed. But my point remains:

You can automate anything, you just have to be creative.