# Infrastructure as Code

Yeah, you know me. 

At my core, I'm an Infrastructure guy. I love the Infrastructure. In my perspective, in order for anything on Earth to function in the best way possible, the Infrastructure has to be up to par with the needs of the future things running on it. You can't always know the future, but you can model and predict where you think things need to go with enough information and data. 

## Terraform 

I love Terraform. 

Personally, I believe that Terraform has the market dominance when it comes to Infrastructure as Code. There are a few others out there, but it's the primary contender. So I'm pretty familiar with Terraform and the ways that you can utilize it to deploy whatever resources have an API. 

### Terraform Experience

Aside from the small-time providers to enable things like `random` or `time`, what are some of the core providers I've worked with?

- [AWS](https://registry.terraform.io/providers/hashicorp/aws/latest)
- [GCP](https://registry.terraform.io/providers/hashicorp/google/latest)
- [Azure](https://registry.terraform.io/providers/hashicorp/azurerm/latest)
- [Azure AD](https://registry.terraform.io/providers/hashicorp/azuread/latest)
- [VMWare](https://registry.terraform.io/providers/hashicorp/vsphere/latest)
- [Artifactory](https://registry.terraform.io/providers/jfrog/artifactory/latest)
- [Gitlab](https://registry.terraform.io/providers/gitlabhq/gitlab/latest)
- [F5/BigIP](https://registry.terraform.io/providers/F5Networks/bigip/latest)
- [Palo Alto / PanOS](https://registry.terraform.io/providers/PaloAltoNetworks/panos/latest)
- [Cisco ASA](https://registry.terraform.io/providers/CiscoDevNet/ciscoasa/latest)
- [Cisco ACI](https://registry.terraform.io/providers/CiscoDevNet/aci/latest)
- [ZScaler ZIA](https://registry.terraform.io/providers/zscaler/zia/latest)
- [Power DNS](https://registry.terraform.io/providers/pan-net/powerdns/latest)

I've used each of them heavily to deploy resources within the various Infrastructure entities.

# Mono-Repo vs. Disjointed Repos

When it comes to Terraform, I think, in general you need a Mono-Repo for the deployment of the Infrastructure, and supporting repositories for version controlled modules that deploy resources within the Mono-Repo.

## But why

I think there's a misnomer when it comes to what a Mono-Repo is. When we think 'Mono' as it comes to Software, it brings back memories of monolithic architectures where everything is tightly coupled, and large software repositories are gigs in size. 

But a mono-repo doesn't _have_ to be a monolithic architecture. 

What a mono-repo does provide is the ability to have a singular pipeline in place that containers your Infrastructure. When users go, "I need a thing!" You have a living example storage space. 

Stack that with never having to ask the question of, **"Who owns this resource?"**

You also end up with a singular pipeline that enables you to perform any and all quality controls, policy checks, and system enhancements, and do them _once_. 

## But why not

Well, with any argument, there needs to be a counter-argument--if there isn't, you don't understand the argument well enough. 

The counter argument to a mono-repo for Infrastructure as Code is that you have a singular location (hopefully) where your entire Infrastructure has been deployed. You wanna talk risk? That thing needs to be some of the most protected resources possible. 

- Who can push
- Who can merge
- Who can review
- Policy Checks
- Standards Reviews
- Security Reviews

If you can't do _all_ of these in a very clear and understood pattern, you shouldn't do a mono-repo. 

# Stay Tuned

At one point I'll write up a design document to showcase how you can leverage an Infrastructure as Code mono-repo, it might even be it's own Gitlab Project people can fork from!